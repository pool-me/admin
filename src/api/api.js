import axios from 'axios'

//Set global configs for axios
let data = {
    baseURL: process.env.VUE_APP_API_BASE_URL
};
let token = null;

export const api = () => {
    token = localStorage.getItem('admin_token');
    axios.defaults.headers.common['Authorization'] = token ? `Bearer ${token}` : null
    return axios.create(data);
};

export default api;
