import Vue from 'vue';
import App from './App.vue';
import store from './store/index';
import { router } from './router';
import axios from 'axios';
// import daterangepicker from 'daterangepicker';
import {ClientTable} from 'vue-tables-2';
Vue.use(ClientTable);

//This is very important to used with the window object;
// window.moment = require('moment');

// import YmapPlugin from 'vue-yandex-maps'
// const settings = {
//   apiKey: '0cd6ff6e-ae8d-4d7c-b6b3-84ff771499b0',
//   lang: 'ru_RU',
//   coordorder: 'latlong',
//   version: '2.1',
// }
// Vue.use(YmapPlugin, settings)

Vue.config.productionTip = false
const token = localStorage.getItem('admin_token');

if (token) {
  axios.defaults.headers.common['Authorization'] = token;
  router.push("/dashboard")
}
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
