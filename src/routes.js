import Dashboard from "./components/LoggedIn/Content/Dashboard/Dashboard";
import Users from "./components/LoggedIn/Content/Users/Users";
import Trips from "./components/LoggedIn/Content/Trips/Trips";
import Passengers from "./components/LoggedIn/Content/Passengers/Passengers";
import Vehicles from "./components/LoggedIn/Content/Vehicles/Vehicles";
import Drivers from "./components/LoggedIn/Content/Drivers/Drivers";
import Statistics from "./components/LoggedIn/Content/Statistics/Statistics";
import Settings from "./components/LoggedIn/Content/Settings/Settings";
export const routes = [
  {
    path: "/dashboard",
    component: Dashboard
  },
  {
    path: "/users",
    component: Users
  },
  {
    path: "/passengers",
    component: Passengers
  },
  {
    path: "/trips",
    component: Trips
  },
  {
    path: "/vehicles",
    component: Vehicles
  },
  {
    path: "/drivers",
    component: Drivers
  },
  {
    path: "/statistics",
    component: Statistics
  },
  {
    path: "/settings",
    component: Settings
  }

];
