// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        passengers: [],
        passenger: null
    },
    actions: {
        getAllPassengers(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getPassengers").then(res => {
                ctx.commit("updatePassengers", res.data);
            })
            .catch(err => console.log(err));
        },
        deletePassenger(ctx,id) {
            axios.post("https://poolme.herokuapp.com/admin/deleteUser/" +id).then(() => {
                ctx.commit("deletePassenger", id);
            })
            .catch(err => console.log(ctx,err));
        }
    },
    mutations: {
        updatePassengers(state, passengers) {
            state.passengers = passengers;
        },
        deletePassenger(state, id) {
            state.passengers.map((passenger,index) => {
               if(passenger.id === id){
                   state.passengers.splice(index, 1);
               }
            })
        }
    },
    getters: {
        getPassengers(state) {
            return state.passengers;
        }
    }
};
