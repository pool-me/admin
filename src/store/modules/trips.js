// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        trips: [],
        cancelledTrips: [],
        bookedTrips: [],
        completedTrips: [],
    },
    actions: {
        getAllTrips(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getTrips").then(res => {
                ctx.commit("updateTrips", res.data);
            }).catch(err => console.log(err));
        },
        getCancelledTripsCount(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getCancelTrips").then(res => {
                ctx.commit("updateCancelledTrips", res.data);
            }).catch(err => console.log(err));
        },
        sendDate() {
            let date = new Date()
            axios.post("https://poolme.herokuapp.com/admin/get",date).then(res => {
            }).catch(err => console.log(err));
        },
        getBookedTripsCount(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getBookedTrips").then(res => {
                ctx.commit("updateBookedTrips", res.data);
            }).catch(err => console.log(err));
        },
        getCompletedTripsCount(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getCompletedTrips").then(res => {
                ctx.commit("updateCompletedTrips", res.data);
            }).catch(err => console.log(err));
        },
        createTrip() {
            // let trip = {
            //     "fromLongitude": "45465",
            //     "fromLatitude" : "1111",
            //     "toLongitude": "1324",
            //     "toLatitude" : "888",
            //     "seats": "22",
            //     "distance" : "12"
            // }
            // let user = {
            //     "username" : "fjdg",
            //     "email" : "fdjcgsjdfg"
            // }
            // axios.post("https://poolme.herokuapp.com/user/createTrip",trip).then(res => {
            //     console.log(res)
            // })
            //     .catch(err => console.log(err));
        },


    },
    mutations: {
        updateTrips(state, trips) {
            state.trips = trips;
        },
        updateTrip(state,trip){
            state.trip = trip;
        },
        updateCancelledTrips(state,trips){
            state.cancelledTrips = trips
        },
        updateBookedTrips(state,trips){
            state.bookedTrips = trips
        },
        updateCompletedTrips(state,trips){
            state.completedTrips = trips
        },
    },
    getters: {
        getTrips(state) {
            return state.trips;
        },
        getTrip(state) {
            return state.trip;
        },
        getCancelledTrips(state){
            return state.cancelledTrips;
        },
        getBookedTrips(state){
            return state.bookedTrips;
        },
        getCompletedTrips(state){
            return state.completedTrips;
        }

    }
};
