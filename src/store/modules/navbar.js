// import axios from "axios";

export default {
  state: {
    activeTab: localStorage.getItem("admin_token") ? "Dashboard" : "" 
  },
  actions: {
    changeTab(ctx, tab) {
      ctx.commit("updateTab", tab);
    }
  },
  mutations: {
    updateTab(state, tabName) {
      state.activeTab = tabName;
    }
  },
  getters: {
    getActiveTab(state) {
      return state.activeTab;
    }
  }
};
