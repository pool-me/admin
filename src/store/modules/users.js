// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        users: [],
        user: {},
        newUsers: [],
        tripStatistics: []
    },
    actions: {
        getAllUsers(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getUsers").then(res => {
                ctx.commit("updateUsers", res.data);
            })
                .catch(err => console.log(err));
        },
        updateUser(ctx,user) {
            axios.post("https://poolme.herokuapp.com/admin/updateUser",user).then(res => {
                ctx.commit("updateUser", res.data);
            })
                .catch(err => console.log(err));
        },
        createUser(ctx,user) {
            axios.post("https://poolme.herokuapp.com/admin/createUser",user).then(res => {
                console.log(res)
            })
                .catch(err => console.log(err));
        },
        getUsersByDate(ctx,date){
            axios.post("https://poolme.herokuapp.com/admin/getUsersByDate" , date).then(res => {
                console.log(res)
                ctx.commit("updateNewUsers", res.data);
            })
             .catch(err => console.log(err));
        },
        getTripsByDate(ctx,startDate,endDate){
            axios.post("https://poolme.herokuapp.com/admin/getTripsByDate" ,startDate,endDate).then(res => {
                console.log(res)
                ctx.commit("updateTripsStatistics", res.data);
            })
                .catch(err => console.log(err));
        }
    },
    mutations: {
        updateUsers(state, users) {
            state.users = users;
        },
        updateUser(state,user){
            state.user = user;
        },
        updateNewUsers(state,users){
            state.newUsers = users
        },
        updateTripsStatistics(state,statistics){
            state.tripStatistics = statistics
        }
    },
    getters: {
        getUsers(state) {
            return state.users;
        },
        getNewUsers(state){
            return state.newUsers
        },
        getTripsStatistics(state){
           return state.tripStatistics
        }
    }
};
