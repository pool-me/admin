// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        drivers: [],
        driver: null
    },
    actions: {
        getAllDrivers(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getDrivers").then(res => {
                ctx.commit("updateDrivers", res.data);
            })
            .catch(err => console.log(err));
        },
        deleteDriver(ctx,id) {
            axios.post("https://poolme.herokuapp.com/admin/deleteUser/" + id).then(() => {
                ctx.commit("deleteDriver", id);
            })
                .catch(err => console.log(ctx,err));
        },
        changeDriverStatus(ctx,user){
            axios.post("https://poolme.herokuapp.com/admin/changeDriverStatus",user).then((res) => {
                console.log(res)
            })
            .catch(err => console.log(ctx,err));
        }
    },
    mutations: {
        updateDrivers(state, drivers) {
            state.drivers = drivers;
        },
        deleteDriver(state,id){
            state.drivers.map((driver,index) => {
                if(driver.id === id){
                    state.drivers.splice(index, 1);
                }
            })
        }
    },
    getters: {
        getDrivers(state) {
            return state.drivers;
        }
    }
};
