import Vue from "vue";
import Vuex from "vuex";
import navbar from "./modules/navbar";
import app from "./modules/app";
import users from "./modules/users";
import passengers from "./modules/passengers";
import drivers from "./modules/drivers";
import trips from "./modules/trips";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    navbar,
    app,
    users,
    passengers,
    drivers,
    trips
  }
});
