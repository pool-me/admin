let myMap;
export default (function () {

    function ViewRoute(trips) {
        ymaps.ready(init);
        function init() {

            myMap = new ymaps.Map("viewRoute", {
                center: [37.785839, -122.406417],
                zoom: 10,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });
            var trafficControl = new ymaps.control.TrafficControl({
                state: {
                    providerKey: 'traffic#actual',
                    trafficShown: false
                }
            });

            myMap.controls.add(trafficControl);
            trafficControl.getProvider('traffic#actual').state.set('infoLayerShown', true);
            trips.map((trip) => {
                        let multiRoute = new ymaps.multiRouter.MultiRoute({
                            referencePoints: [
                                [parseFloat(trip.fromLatitude), parseFloat(trip.fromLongitude)],
                                [parseFloat(trip.toLatitude), parseFloat(trip.toLongitude)]
                            ],
                            params: {
                                results: 2
                            }
                        }, {
                            boundsAutoApply: true
                        });
                        myMap.geoObjects.add(multiRoute);

                    })
        }



    }

    return ViewRoute;
}());

