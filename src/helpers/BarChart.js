import { Bar, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Bar,
  mixins: [reactiveProp],
  props: {
      chartData: {
          type: Object,
          default: () => {
              return null
          }
      },
      options: {
          type: Object,
          default: () => {
              return null;
          }
      }
  },
  mounted () {
    // this.chartData создаётся внутри миксина.
    // Если вы хотите передать опции, создайте локальный объект options
    this.renderChart(this.chartData, this.options)
  }
}