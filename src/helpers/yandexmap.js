let myMap;
export default (function () {

    function Map(trips, showRoutes = false) {

        function init() {

            myMap = new ymaps.Map("map", {
                center: [37.785839, -122.406417],
                zoom: 10,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });
            var trafficControl = new ymaps.control.TrafficControl({
                state: {
                    providerKey: 'traffic#actual',
                    trafficShown: false
                }
            });

            myMap.controls.add(trafficControl);
            trafficControl.getProvider('traffic#actual').state.set('infoLayerShown', true);
            trips.map((trip) => {
                let inst = this;
                myMap.geoObjects.add(new ymaps.Placemark([parseFloat(trip.fromLatitude), parseFloat(trip.fromLongitude)], {
                    balloonContentHeader: trip.statusTrips
                }, {
                    preset: 'islands#circleIcon',
                    iconLayout: 'default#image',
                    // Custom image for the placemark icon.
                    iconImageHref: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Circle-icons-car.svg/1200px-Circle-icons-car.svg.png',
                    // The size of the placemark.
                    iconImageSize: [50, 50],
                    /**
                     * The offset of the upper left corner of the icon relative
                     * to its "tail" (the anchor point).
                     */
                    iconImageOffset: [-5, -38]
                }))

            })
        }

            ymaps.ready(init);

    }

    return Map;
}());

