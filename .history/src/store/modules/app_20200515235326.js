// import api from "../../api/api";
import axios from 'axios';
export default {

  state: {
    token: localStorage.getItem("admin_token") ? localStorage.getItem("admin_token") : null,
    status: ""
  },
  actions: {
    auth(ctx,form) {
      ctx.commit("authRequest");
      axios.post("https://poolme.herokuapp.com/admin/signIn",form).then(res => {
          const userToken = res.data.token ? res.data.token : "admin_auth_success";
          localStorage.setItem("admin_token", userToken);
          ctx.commit("updateToken", userToken);
        })
        .catch(err => console.log(err));
    },,
    auth(ctx,form) {
      ctx.commit("authRequest");
      axios.post("https://poolme.herokuapp.com/admin/signIn",form).then(res => {
          const userToken = res.data.token ? res.data.token : "admin_auth_success";
          localStorage.setItem("admin_token", userToken);
          ctx.commit("updateToken", userToken);
        })
        .catch(err => console.log(err));
    },
    logOut(ctx) {
      localStorage.clear();
      ctx.commit("updateToken", "");
    }
  },
  mutations: {
    updateToken(state, token) {
      state.token = token;
      state.status = "success";
    },
    authRequest(state) {
      state.status = "loading";
    },
    authError(state, err) {
      state.status = err;
    }
  },
  getters: {
    getToken(state) {
      return state.token;
    }
  }
};
