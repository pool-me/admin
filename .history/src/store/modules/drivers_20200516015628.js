// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        drivers: [],
        driver: null
    },
    actions: {
        getAllDrivers(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getDrivers").then(res => {
                ctx.commit("updateDrivers", res.data);
            })
            .catch(err => console.log(err));
        },
    },
    mutations: {
        updateDrivers(state, drivers) {
            state.drivers = drivers;
        },
    },
    getters: {
        getDrivers(state) {
            return state.drivers;
        }
    }
};
