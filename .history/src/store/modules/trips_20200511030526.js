// import api from "../../api/api";
import axios from 'axios';
export default {

    state: {
        trips: []
    },
    actions: {
        getAllTrips(ctx) {
            axios.post("https://poolme.herokuapp.com/admin/getTrips").then(res => {
                ctx.commit("updateTrips", res.data);
            })
                .catch(err => console.log(err));
        },
        findTrips(){
            let trip = {
                "fromLongitude": "37.785834",
                "fromLatitude" : "37.785834",
                "toLongitude": "-122.206417",
                "toLatitude" : "37.585834",
            }
            axios.post("https://poolme.herokuapp.com/user/findTrips",trip).then(res => {
                console.log(res)
            })
                .catch(err => console.log(err));
        },
        createTrip() {
            let trip = {
                "fromLongitude": "45465",
                "fromLatitude" : "1111",
                "toLongitude": "1324",
                "toLatitude" : "888",
                "seats": "22",
                "distance" : "12"
            }
            // let user = {
            //     "username" : "fjdg",
            //     "email" : "fdjcgsjdfg"
            // }
            axios.post("https://poolme.herokuapp.com/user/createTrip",trip).then(res => {
                console.log(res)
            })
                .catch(err => console.log(err));
        },

    },
    mutations: {
        updateTrips(state, trips) {
            state.trips = trips;
        },
        updateTrip(state,trip){
            state.trip = trip;
        }
    },
    getters: {
        getTrips(state) {
            return state.trips;
        },
        getTrip(state) {
            return state.trip;
        }
    }
};
